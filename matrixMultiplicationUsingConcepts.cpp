#include<cassert>
#include<iostream>
#include<string>
#include<vector>
		
template< typename T >
concept integer = std::is_integral_v< T >;

template< typename T >
concept arithmetic = std::is_arithmetic_v< T >;

template < integer rows, integer cols, arithmetic matrixx >
struct vector
{
  rows _r;
  cols _c;
  std::vector< matrixx > _elemetns;
  vector( rows r, cols c, std::vector< matrixx >&& elements )
    :  _r( r )
    ,  _c( c )
    ,  _elemetns( std::move( elements ) ) 
    { 
    }
};

template < integer rows, integer cols, arithmetic matrixx >
struct matrix
{
  rows _r;
  cols _c;
  std::vector< matrixx > _elemetns;
  matrix( rows r, cols c, std::vector< matrixx >&& elements )
    :  _r( r )
    ,  _c( c )
    ,  _elemetns( std::move( elements ) ) 
    { 
    }

  auto operator+( const auto& vec )
  {
    int i=0;
    assert( this->_r == vec._r && this->_c == vec._c );
    matrix< rows, cols, matrixx > result( this->_r, this->_c, {} );

    while( i < this->_r * this->_c )
    {
      result._elemetns.emplace_back( vec._elemetns[ i ] + this->_elemetns[ i ] );
      ++i;
    }
    return result;
  }

  auto operator*( vector< rows, cols, matrixx >& vec )
  {
    unsigned i=0;
    matrixx elements = 0;
    matrix< rows, cols, matrixx > result { this->_r, vec._c, {} };

    while( i < this->_r * this->_c )
    {
      for ( int j=0; j<this->_c; ++j )
        elements += this->_elemetns[ i+j ]*vec._elemetns[ j ];
      result._elemetns.emplace_back( elements );
      i=i+this->_c;
      elements = 0;
    }
    return result;
  }

  auto operator*( matrixx scalar )
  {
    int i=0;
    matrix< rows, cols, matrixx > result { this->_r, this->_c, {} };
    while( i < this->_r * this->_c )
    {
      result._elemetns.emplace_back( scalar*this->_elemetns[ i ] );
      ++i;
    }
    return result;
  }
};

int main( int argc, const char* argv[] )
{
  double scalar = 10.5;
  auto print = [ i=1 ]( auto obj ) mutable {
    std::cout << "[ " << obj._r << " * " << obj._c << " ]\n";
    for ( const auto& itr : obj._elemetns )
    {
      std::cout << itr << "\t";
      if ( i==obj._c )
      {
        std::cout << "\n";
        i=0;
      }
      ++i;
    }
    std::cout << "************\n";
  };
  
  matrix< int,int, double > m0 ( 2, 2, { 1.1 ,2.2 ,3.3 ,4.4 } );
  vector< int,int, double > v0 ( 2, 1, { 1.1 ,2.2 } );
  auto mul1 = m0*v0;
  print( mul1 );
  auto scalarMul = mul1*scalar;
  print( scalarMul );
  auto add1 = mul1+v0;
  print( add1 );


}
