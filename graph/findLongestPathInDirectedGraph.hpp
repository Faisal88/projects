#ifndef FINDLONGESTPATHINDIRECTEDGRAPH_HPP
#define FINDLONGESTPATHINDIRECTEDGRAPH_HPP

#include <fstream>
#include <map>
#include <set>
#include <vector>

struct Edge { int _src, _dest, _weight; };
struct Node { int _vertex, _weight; std::set<int> _set; };

using mapSet = std::map<int, std::set< int > >;
using edgeVector = std::vector< Edge >;
using VedgeVector = std::vector< std::vector< Edge > >;
using rItSet = std::set< int >::reverse_iterator;

struct Graph
{
  VedgeVector _adjList; // adjacency List
  unsigned _size;
  Graph( edgeVector const &edges, const unsigned N );
  friend std::ostream& operator << ( std::ostream& os, const Graph& graphObj );
  mapSet removeMostExpensivePathFromGraph( int src );
  void longestPathToBackwards( mapSet& costOfVertices );
};

#endif // FINDLONGESTPATHINDIRECTEDGRAPH_HPP

