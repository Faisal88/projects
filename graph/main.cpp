#include <iostream>
#include <queue>

#include "findLongestPathInDirectedGraph.hpp"

Graph::Graph( edgeVector const &edges, const unsigned N )
  : _size( N )
{
  _adjList.resize( N );
  for ( auto &edge: edges )
  _adjList[ edge._src ].push_back( { edge._src, edge._dest, edge._weight } );
}

std::ostream& operator << ( std::ostream& os, const Graph& graphObj )
{
  os << "\nFrom,To,Weight\n";
  for ( int i=0; i < graphObj._size; ++i )
    for ( const auto vec : graphObj._adjList[ i ] )
      os << vec._src << "," << vec._dest << "," << vec._weight << std::endl;
  return os;
}

mapSet Graph::removeMostExpensivePathFromGraph( int src )
{
  std::queue< Node > q;
  std::set< int > vertices;
  vertices.insert( src );
  q.push( { src, 0, vertices } );

  mapSet costOfVertices;
  int tempCost=0;
  while ( !q.empty() )
  {
    Node node = q.front();
    q.pop();
    int v = node._vertex;
    int cost = node._weight;
    vertices = node._set;
    for ( const auto& edge : this->_adjList[ v ] )
    {
     std::set< int > setOfVertices = vertices;
     setOfVertices.insert( edge._dest );
     q.push( { edge._dest, cost + edge._weight, setOfVertices } );
     costOfVertices.insert( { cost + edge._weight, setOfVertices } );
     int maxCost = cost + edge._weight;
     if ( tempCost < maxCost )
       tempCost = maxCost;
    }
  }

  auto indices = costOfVertices.find( tempCost );
  int arr[ indices->second.size() ];
  static int i=0;
  for ( auto elements : indices->second )
  {
    arr[ i ] = elements;
    ++i;
    if ( i == indices->second.size() )
      break;
  }

  auto obj = this->_adjList;
  for ( int i= 0; i < indices->second.size()-1; ++i )
  {
    for ( auto x : obj[ arr[ i ] ] )
    {
      if ( x._src == arr[ i ] && x._dest == arr[ i+1 ] )
      {
        auto it = std::begin( obj[ arr[ i ] ] );
        for( int j = 0; j < obj[ arr[ i ] ].size(); ++j )
        {
          if ( !( it->_src == arr[ i ] && it->_dest ==arr[ i+1 ] ) )
          {
            std::advance( it, 1 );
          }
          else
          {
            obj[ arr[ i ] ].erase( it );
            break;
          }
        }
      }
    }
  }

  std::cout << "\nAfter First Run\n";

  for ( int i=0; i < obj.size(); ++i )
    for ( const auto vec : obj[ i ] )
      std::cout  << vec._src << "," << vec._dest << "," << vec._weight << std::endl;

  costOfVertices.erase( tempCost );
  return costOfVertices;
}

void Graph::longestPathToBackwards( mapSet& costOfVertices )
{
  int secondHighestCost[ costOfVertices.size() ];
  static int i = 0;
  for ( auto keys : costOfVertices )
  {
    secondHighestCost[ i ] = keys.first;
    if ( i == costOfVertices.size()-1 )
      break;
    ++i;
  }

  std::set< int > s = costOfVertices.find( secondHighestCost[ costOfVertices.size()-1] )->second;
  rItSet rit;
  std::cout << "\nThe longest Path as backwards: ";
  for ( rit = s.rbegin(); rit != s.rend(); rit++ )
    std::cout << *rit << " ";
  std::cout << std::endl;
}

int main()
{
  edgeVector edges =
  {
    { 1,2,1 }, { 1,3,2 },
    { 3,4,4 }, { 2,4,7 },
    { 1,5,1 }, { 2,6,4 },
    { 5,6,3 }
  };

  Graph graph( edges, edges.size() );
  std::cout << graph;

  auto graphWithoutMostExpensivePath = graph.removeMostExpensivePathFromGraph( edges.front()._src );
  graph.longestPathToBackwards( graphWithoutMostExpensivePath );
}

