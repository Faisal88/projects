#include<cassert>
#include<iostream>
#include<string>
#include<vector>

template < int Rows, int Cols, typename T >
struct vector
{
  unsigned _rows, _cols;
  std::vector< T > _vec;
  vector( std::vector< T >&& v  )
    : _rows( Rows )
    , _cols( Cols )
    , _vec ( std::move( v ) ) {}
};

template< int Rows, int Cols, typename T >
struct matrix
{
  unsigned _rows, _cols;
  std::vector< T > _vec;
  matrix()
    : _rows( 0 )
    , _cols( 0 ) {}
  matrix( const std::vector< T >&& v  )
    : _rows( Rows )
    , _cols( Cols )
    , _vec( std::move( v ) ) {}

  auto operator*( auto& vec )
  {
    assert( this->_cols == vec._rows );
    unsigned i=0;
    int elements = 0;
    matrix<Rows, Cols, T > result;
    result._rows = this->_rows;
    result._cols = vec._cols;
    while( i < this->_rows*this->_cols )
    {
      for ( int j=0; j<this->_cols; ++j )
        elements += this->_vec[ i+j ]*vec._vec[ j ];
      result._vec.emplace_back( elements );
      i=i+this->_cols;
      elements = 0;
    }
    return result;
  }

  auto operator*( T scalar )
  {
    int i=0;
    matrix<Rows, Cols, T> result;
    result._rows = this->_rows;
    result._cols = this->_cols;
    while( i < this->_rows*this->_cols )
    {
      result._vec.emplace_back( scalar*this->_vec[ i ] );
      ++i;
    }
    return result;
  }

  auto operator+( const auto& vec )
  {
    int i=0;
    assert( this->_rows == vec._rows && this->_cols == vec._cols );
    matrix<Rows, Cols, T > result;
    result._rows = this->_rows;
    result._cols = this->_cols;
    while( i < this->_rows*this->_cols )
    {
      result._vec.emplace_back( vec._vec[ i ]+this->_vec[ i ] );
      ++i;
    }
    return result;
  }
};

int main()
{
  int scalar = 10;   // for scalar multiplication
  matrix< 2,2,int > m0{ std::vector< int >{ 1,2,3,4 } };
  vector< 2,1,int > v0{ std::vector< int >{ 1,2 } }; 

  auto print = [ i=1 ]( auto obj ) mutable {
    std::cout << "[ " << obj._rows << " * " << obj._cols << " ]\n";
    for ( const auto& itr : obj._vec )
    {
      std::cout << itr << "\t";
      if ( i==obj._cols )
      {
        std::cout << "\n";
        i=0;
      }
      ++i;
    }
    std::cout << "************\n";
  };

  auto mul = m0*v0;
  print( mul );
  auto scalarMul = m0*scalar;
  print( scalarMul );
  auto add = m0+scalarMul;
  print( add );
}
