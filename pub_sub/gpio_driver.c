// GPIO Driver for Emulation purpose
//  Connects SUB socket to tcp://172.17.0.4:5556

#include "zhelpers.h"
#include <string.h>
#include <stdlib.h>
#include <wiringPi.h>

#define WoW_DRIVER 0 // WiringPi pin #
#define PA_DRIVER 1
#define DOCKER_IP_PORT "tcp://172.17.0.1:5555"

int main (int argc, char *argv [])
{
    //  Socket to talk to server
    printf ("Emulator for Pull up Resistors GPIO #17 & 18…\n");
    void *context = zmq_ctx_new ();
    void *subscriber = zmq_socket (context, ZMQ_SUB);
    int rc_sub = zmq_connect (subscriber, DOCKER_IP_PORT);
	//printf("%d \n", rc_sub);
	//if(rc_sub!=0) printf("assertion failed!");
    assert (rc_sub == 0);
    rc_sub = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE,
                         "aero.paxlife.flightapi.PXI4223",30);

	// GPIO Driver setup
	if(wiringPiSetup() == -1){
		printf("Error: Unable to setup wiringPi\n");
		return 1;
	}
	
	pinMode(WoW_DRIVER, OUTPUT);
	pinMode(PA_DRIVER, OUTPUT);


    assert (rc_sub == 0);
	while(1) { 
        char *string1 = s_recv (subscriber);
        free (string1);	
	string1 = s_recv (subscriber);
	//printf("String Received: %s\n",string1);

	// Timestamp,UTC,Callsign,Position,Altitude,Speed,Direction
	char* Timestamp= strtok(string1, ",");
	char* UTC= strtok(NULL, ",");
	char* Callsign= strtok(NULL, ",");
	char* PositionLat= strtok(NULL, ",");
	char* PositionLong= strtok(NULL, ",");
	char* Altitude= strtok(NULL, ",");
	char* Speed= strtok(NULL, ",");
	char* Direction= strtok(NULL, "\n");

	//printf("AfterTok: %s %s %s %s %s %s %s %s \n\n", Timestamp,UTC,Callsign,PositionLat,PositionLong,Altitude,Speed,Direction);
	int altitude= atoi(Altitude);
	int speed= atoi(Speed);

	if(altitude  > 0){
		digitalWrite(WoW_DRIVER, 0);
		printf("WoW = 0\n");
	}
	else{
		digitalWrite(WoW_DRIVER, 1);
		printf("WoW = 1\n");
	}
		
        free (string1);
}
    zmq_close (subscriber);

    zmq_ctx_destroy (context);
    return 0;
}
