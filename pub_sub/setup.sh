#!/bin/bash
echo "Installing docker, awscli, libczmq and build-essential"
sudo apt-get -y install docker.io > /dev/null
sudo apt-get -y install awscli > /dev/null
sudo apt-get -y install libczmq-dev > /dev/null
sudo apt-get -y install build-essential > /dev/null

#aws configure
mkdir ~/.aws
cp awscli-config/credentials ~/.aws
cp awscli-config/config ~/.aws
sudo $(aws ecr get-login --registry-ids 217233105762)

sudo docker run -p 5555:5555 217233105762.dkr.ecr.eu-central-1.amazonaws.com/pxi/challenge/flightapi:arm
