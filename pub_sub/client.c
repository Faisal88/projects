/* 
This is a Client + Server (SUB and PUB) file that receives the data from the docker which acts as a Publisher and broadcasting the flight
status..This file is also used to initialize the Raspberry Pi 3 B V1.2 and emulates the Weight on Wheel value and 
Passenger Announcement  by setting the GPIO pins initially to default values and passed as output to another two GPIO Pins 
*/

//  Connects SUB socket to tcp://172.17.0.4:5556

#include "zhelpers.h"	// Library to set \0 during string formation when using s_rec() and s_send()
#include <string.h>	// This library is used to remove the delimiter i.e. "," of the published data that is received as CSV format with strtok().
#include <stdlib.h>
#include <limits.h>	// This library is useed to use the maximum limit of int in case the pilot  wants to fly higher  
#include <wiringPi.h>	// C library to access GPIO pins 

// The WoW_DRIVER and PA_DRIVER values are fed as Output Values into the following pins.
#define WoW_INPUT 25 // WiringPi pin #
#define PA_INPUT 28
#define DOCKER_IP_PORT "tcp://172.17.0.1:5555"


// Variables created for comparison with the current values of altitude and speed to effeciently calculate flight phases.
int prev_altitude=0;
int prev_speed=0;

int main (int argc, char *argv [])
{
	//  S+ocket to talk to server
   	printf ("Client in SUB mode …\n");

   	void *context = zmq_ctx_new (); // context created i.e. nedd to created only once for IPC
 	void *subscriber = zmq_socket (context, ZMQ_SUB); // Subscriber socket created
	int rc_sub = zmq_connect (subscriber, DOCKER_IP_PORT); // Client socket uses a connect () and bind() is for Server socket 

	assert (rc_sub == 0);
	// To receive the data, we must first subscribe. As the publisher is broadcasting to many clients (if there are more than one but here its
	// only one) and every clients has its own need, therefore the filter must be set by using setsockopt(). The filter below receives all the data
	// coming from the aero paxlife flight
	rc_sub = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE,
                         "aero.paxlife.flightapi.PXI4223",30); 

	// enum is used to detect change in flight phase difference..
	enum flight_phase_t {GATE, TAXI, TAKEOFF_ROLL, TAKEOFF, CLIMB, CRUISE, DESCENT, UNDEFINED} phase, prev_phase;

	// default values	
	prev_phase = UNDEFINED;
	phase = UNDEFINED;

	// Socket to talk to another client responsible to receive the timestamp values (+ PA etc) when there is a change flight phases
	void *publisher = zmq_socket (context, ZMQ_PUB); // Publisher socket created
	int rc_pub = zmq_bind (publisher, "tcp://*:5556"); // Server needs a bind() to listen 
	assert (rc_pub == 0);
	assert (rc_sub == 0);

	// GPIO Setup
	if(wiringPiSetup() == -1){
		printf("Error: Unable to setup wiringPi\n");
		return 1;
	}	

	while(1) { 
		
		// Receiving data from the publisher
        	char *string1 = s_recv (subscriber);	 
        	free (string1);
		string1 = s_recv (subscriber);
		
		//printf("Published Data: Received:\n %s\n WoW: %d\n PA: %d\n",string1, WoW, PA); 

		// Removing the delimiter: Timestamp,UTC,Callsign,Position,Altitude,Speed,Direction
		char* Timestamp= strtok(string1, ",");
		char* UTC= strtok(NULL, ",");
		char* Callsign= strtok(NULL, ",");
		char* PositionLat= strtok(NULL, ",");
		char* PositionLong= strtok(NULL, ",");
		char* Altitude= strtok(NULL, ",");
		char* Speed= strtok(NULL, ",");
		char* Direction= strtok(NULL, "\n");

		// printf("AfterTok: %s %s %s %s %s %s %s %s \n\n", Timestamp,UTC,Callsign,PositionLat,PositionLong,Altitude,Speed,Direction);	
		
		int altitude= atoi(Altitude);
		int speed= atoi(Speed);
		int timestamp= atoi(Timestamp);

		pinMode(WoW_INPUT, INPUT);
		pinMode(PA_INPUT, INPUT);
		bool WoW= digitalRead(WoW_INPUT); 	//Reading from the WOW_PIN (#25) 
		bool PA= digitalRead(PA_INPUT); 		//Reading from the WOW_PIN (#28)
		 printf("WoW: %d\nAltitude: %d \nSpeed: %d \n\n",WoW, altitude,speed);

		// Calculating Flight Phases 
		switch(speed){
			case 0 ... 30 :
				if (WoW && (altitude==0))
					if (speed==0) 
					{
						phase=GATE;
						 // printf("Flight to Gate\n"); 
					}
					else { 
						phase=TAXI; 
						// printf("Flight to Taxi\n"); 
					};
			break;
				
			case 31 ... INT_MAX:
				if (WoW && (altitude==0 )) {
					if (prev_speed < speed) {
						phase=TAKEOFF_ROLL;
						// printf("TakeOFF_Roll\n");
					}
				}
				else	{
					if (altitude>0 && altitude<1000 && (phase==TAKEOFF_ROLL)) { 
						if (altitude>prev_altitude) {					
						phase= TAKEOFF;
	    					// printf("Taking Off\n");
						}
					}
	    				else if (prev_altitude<altitude )  {
						if (altitude > 1000 && altitude<=30000) {					
						phase= CLIMB;					
						// printf("Climb and Increasing\n");
						}
					}
	    				else if (altitude > 30000) { 
						phase= CRUISE;
	            				// printf("Cruise and Stable\n");
					}
					else if (prev_altitude>altitude) { 
						if (altitude < 30000 && altitude>0) {
						phase= DESCENT;
	            				// printf("Descent and Decreasing\n");
						}
					}
				}
		break;
		}	

        free (string1);

	//Only Publishing Data, when there is a change in flight phase. 
	if (phase != prev_phase) {	
		char FLIGHT_PHASE_STRING= phase;
		char newData [70];
	        sprintf (newData, "Flight_Phase: %d\nTimestamp: %d\nPassenger_Announcement: %d \nChange", FLIGHT_PHASE_STRING, timestamp, PA);		
		s_send (publisher, newData);
	}
	prev_phase = phase;
	prev_altitude= altitude;
	prev_speed= speed;
}
    zmq_close (subscriber);
    zmq_close (publisher);

    zmq_ctx_destroy (context);
    return 0;
}
