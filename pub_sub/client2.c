//  Connects SUB socket to tcp://localhost:5556

#include "zhelpers.h"

int main (int argc, char *argv []) {
	printf ("Phase 0 is Gate\nPhase 1 is Taxi\nPhase 2 is Takeoff_Roll\nPhase 3 is Takeoff\nPhase 4 is Climb and Increasing\nPhase 5 is Cruise\nPhase 6 is Descent and Decreasing\n");
	void *context = zmq_ctx_new ();
	void *subscriber = zmq_socket (context, ZMQ_SUB);
	int rc = zmq_connect (subscriber, "tcp://localhost:5556");
	assert (rc == 0);
	// No filter is used
	 rc = zmq_setsockopt (subscriber, ZMQ_SUBSCRIBE,
                        "", 0); 
	assert (rc == 0);
   	while(1) {
		char newdata[50];
 	       char *string = s_recv (subscriber);
 		printf ("\n\n%s", string);
        	free (string);
    	}
    	zmq_close (subscriber);
    	zmq_ctx_destroy (context);
return 0;
}
