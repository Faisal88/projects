#include <iostream>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

void print(boost::system::error_code const &e)
{
  std::cout << "After Timer" << std::endl;
}

int main()
{
  boost::asio::io_service io;

  boost::asio::deadline_timer t(io, boost::posix_time::seconds(2));

  std::cout << "Before Timer" << std::endl;
  t.async_wait(&print);

  io.run();

  return 0;
}

