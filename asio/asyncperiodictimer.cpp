#include <iostream>
#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

class async_timer {
public:
  async_timer(
        boost::asio::io_service &io_service,
        boost::asio::deadline_timer::duration_type const &duration,
        int iterations)
    : m_timer(io_service, duration)
    , m_duration(duration)
    , m_iteration(0)
    , m_maxIterations(iterations)
  {
    m_timer.async_wait(std::bind(&async_timer::print, this, std::placeholders::_1));
  }

  void print(boost::system::error_code const &e)
  {
    std::cout << "Timer Iteration " << m_iteration << std::endl;
    m_iteration++;

    if (m_iteration < m_maxIterations)
    {
      m_timer.expires_from_now(m_duration);
      m_timer.async_wait(std::bind(&async_timer::print, this, std::placeholders::_1));
    }
  }

private:
  boost::asio::deadline_timer m_timer;
  boost::asio::deadline_timer::duration_type m_duration;
  int m_iteration;
  int m_maxIterations;

};

int main()
{
  boost::asio::io_service io_service;

  std::cout << "Before Timer" << std::endl;
  async_timer(io_service, boost::posix_time::seconds(1), 5);

  io_service.run();
  std::cout << "Run returned." << std::endl;

  return 0;
}

