#include <iostream>
#include <boost/asio.hpp>

#include <boost/date_time/posix_time/posix_time.hpp>

int main()
{
  boost::asio::io_service io;
  boost::asio::deadline_timer t(io, boost::posix_time::seconds(2));

  std::cout << "Before Timer" << std::endl;
  t.wait();
  std::cout << "After Timer" << std::endl;

  return 0;
}


