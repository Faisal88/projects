#include <iostream>
#include <string>
#include <memory>
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>

int main()
{
  try
  {
    typedef boost::asio::ip::udp asioudp;
    boost::asio::io_service io_service;

    asioudp::socket socket(io_service, asioudp::endpoint(asioudp::v4(), 13));

    while (true)
    {
      std::array<char, 256> recv_buf;
      asioudp::endpoint remote_endpoint;
      boost::system::error_code error;

      std::size_t const received_bytes = socket.receive_from(
        boost::asio::buffer(recv_buf),
        remote_endpoint,
        0,
        error);

      if (error && error != boost::asio::error::message_size)
        throw boost::system::system_error(error);

      std::string const inputmessage(recv_buf.data(), received_bytes);
      std::cout << "Client sent message: \"" << inputmessage << "\"" << std::endl;

      std::string const message = "hello client.\n";

      boost::system::error_code ignored_error;
      socket.send_to(
        boost::asio::buffer(message),
        remote_endpoint,
        0,
        ignored_error);
    }
  }
  catch (std::exception const &e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}

