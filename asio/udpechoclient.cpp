#include <iostream>
#include <string>
#include <memory>
#include <boost/asio/buffer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/udp.hpp>

int main(int argc, char *argv[])
{
  try
  {
    typedef boost::asio::ip::udp asioudp;
    if (argc != 2)
    {
      std::cerr << "Usage: client <host>" << std::endl;
      return 1;
    }

    boost::asio::io_service io_service;

    asioudp::endpoint receiver_endpoint = asioudp::endpoint(
            boost::asio::ip::address_v4::from_string(argv[1]),
            13);

    asioudp::socket socket(io_service);
    socket.open(asioudp::v4());

    std::string const send_buf = "hello server.";
    socket.send_to(boost::asio::buffer(send_buf), receiver_endpoint);

    std::array<char, 128> recv_buf;
    asioudp::endpoint sender_endpoint;
    size_t const len = socket.receive_from(
        boost::asio::buffer(recv_buf), sender_endpoint);

    std::string const received_message(recv_buf.data(), len);
    std::cout << "received from server: \"" << received_message << "\"" << std::endl;
  }
  catch (std::exception const &e)
  {
    std::cerr << e.what() << std::endl;
  }

  return 0;
}

