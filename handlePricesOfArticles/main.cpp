#include "priceHandling.h"
#include "articlesLists.h"

int main()
{
  articlesLists list;
  double laptop = list.getPrice( "laptop" );
  double playstation = list.getPrice( "playstation" );
  priceHandling usd1( currency::usd, laptop, 0 );
  priceHandling usd2( currency::usd, playstation, 0 );
  priceHandling usd3 = usd1 + usd2;

  // adding 2 similar types, including taxes
  std::cout << "From List: " << usd3 << usd3.getSymbol() << std::endl;

  // conversion to euro
  priceHandling eur1 = usd3.convertCurrency( currency::eur );
  std::cout << "After conversion: "<< eur1 << eur1.getSymbol() << std::endl;
}

