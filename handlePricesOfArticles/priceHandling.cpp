#include <cassert>
#include "priceHandling.h"

priceHandling::
priceHandling( currency type, int wholeAmount = 0, int fractionalAmount = 0 )
  : _type( type )
{
  _whole = wholeAmount;
  _fractional = fractionalAmount;
  _currency[ currency::eur ] = 0.7494;
  _currency[ currency::usd ] = 1.3343;
  _symbol = std::make_pair< std::string, std::string >( "$", "€" );
}

priceHandling priceHandling::convertCurrency( const currency convertTo )
{
  priceHandling res( currency::none );
  int key = convertTo;

  if( _currency.find( key ) != _currency.end() )
  {
    double conversion = _currency[ key ];
    conversion *=  _whole;
    res._whole = conversion;
    res._fractional = _fractional;
    res._type = convertTo;
  }
  return res;
}

priceHandling priceHandling::operator + ( const priceHandling& obj )
{
  assert( this->_type == obj._type );
  priceHandling res( currency::none );
  int sum = _fractional + obj._fractional;
  int newFrac = static_cast< int >( sum ) % 100;
  int carry = sum > 100 ? 1 : 0;
  res._whole =  _whole + obj._whole + carry;
  res._fractional = static_cast<int>( newFrac );
  res._type = this->_type;
  res.includeTaxes();
  res.externalTaxes();
  return res;
}

priceHandling priceHandling::operator - ( const priceHandling& obj )
{
  assert( this->_type == obj._type );
  priceHandling res( currency::none );
  int newFrac = ( _fractional - obj._fractional ) % 100;
  int carry = newFrac < 0 ? -1 : 0;
  newFrac = newFrac < 0 ? 100 + newFrac : newFrac;
  res._whole = _whole - obj._whole + carry;
  res._fractional = newFrac;
  res._type = this->_type;
  res.includeTaxes();
  res.externalTaxes();

  return res;
}

std::ostream& operator << ( std::ostream& os, const priceHandling& obj )
{
  if ( obj._type == currency::usd )
    os << obj._whole << "." << obj._fractional << " ";
  else if ( obj._type == currency::eur )
    os << obj._whole << "," << obj._fractional << " ";
  return os;
}

priceHandling priceHandling::includeTaxes()
{
  switch ( _type )
  {
    case currency::usd :
    case currency::eur :
      this->_whole += ( ( this->_whole*10 ) / 100 );
      return *this;
  }
}

priceHandling priceHandling::externalTaxes()
{
  switch ( _type )
  {
    case currency::usd :
    case currency::eur :
      this->_whole += ( ( this->_whole*7 ) / 100 );
    return *this;
  }
}

int priceHandling::whole()
{
  return _whole;
}

int priceHandling::fractional()
{
  return _fractional;
}

std::string priceHandling::getSymbol()
{
 if ( _type == currency::usd )
   return std::get<0>( _symbol );
 else
    return std::get<1>( _symbol );
}

