#ifndef PRICEHANDLING_H
#define PRICEHANDLING_H

#include <iostream>
#include <map>

enum currency {
  usd =1, eur, none
};

struct priceHandling
{
  priceHandling( currency type, int wholeAmount, int fractionalAmount );
  priceHandling convertCurrency( const currency convertTo );

  priceHandling operator + ( const priceHandling& obj );
  priceHandling operator - ( const priceHandling& obj );
  friend std::ostream& operator << ( std::ostream& os, const priceHandling& obj );

  std::string getSymbol();
private:
  currency _type;
  int _whole;
  int _fractional; // penny, cents
  std::map<int, double> _currency;
  std::pair<std::string, std::string> _symbol;

  // assuiming USA and euro zone has same tax system
  priceHandling includeTaxes();   //  10 %
  priceHandling externalTaxes();  //  7 %

  int whole();
  int fractional();
};

#endif // PRICEHANDLING_H

