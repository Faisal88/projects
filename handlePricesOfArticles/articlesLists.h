#ifndef ARTICLESLISTS_H
#define ARTICLESLISTS_H

#include <map>

class articlesLists
{
public:
  double  getPrice( const std::string& name );
  std::string getsymol( const std::string & name );
  articlesLists();
  articlesLists ( const articlesLists& );
  articlesLists ( const articlesLists&& );
  articlesLists& operator = ( articlesLists& );
  articlesLists& operator = ( articlesLists&& );

private:
  std::map< std::string, std::pair< double, std::string > > _article;
};

#endif // ARTICLESLISTS_H

