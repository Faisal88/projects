#include <fstream>
#include <iostream>

#include "articlesLists.h"

double articlesLists::getPrice( const std::string & name )
{
  double price;
  std::tie( price, std::ignore ) = _article[ name ];
  return price;
}

std::string articlesLists::getsymol( const std::string & name )
{
  std::string symbol;
  std::tie( std::ignore, symbol ) = _article[ name ];
  return symbol;
}

articlesLists::articlesLists()
{
  std::ifstream ifs( "articlesList.txt" );
  std::string name, _price;
  int price;
  std::string symbol;
  while( getline( ifs, name, ' ' ) )
  {
    getline( ifs, _price, ' ' );
    price = std::stoi( _price );
    getline( ifs, symbol );
    _article[ name ] = std::make_pair( price, symbol );
  }
}

